package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class LligaDatabase {
    private static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/";
    private static String BD = "rvtfscby";
    private static String USER = "rvtfscby";
    private static String PASS = "J6m0DkO0ckbbpHci6a8j4UO6zaf63160";


    private static LligaDatabase database = null;
    public static LligaDatabase getInstance(){
        if(database == null)
            database = new LligaDatabase();
        return database;
    }

    private Connection connection;

    public LligaDatabase(){
        connection = null;
    }
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL + BD, USER, PASS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public void close(){
        try {
            connection.close();
        }catch(SQLException e){
            System.err.println("Error tancant la BD");
        }
        connection = null;
    }

}
