package data;

import models.Equip;
import models.Partit;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PartitDAO {
    private EquipDAO equipDAO;

    public PartitDAO() {
        equipDAO = new EquipDAO();
    }

    public void insert(Partit p){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "insert into partit (equip_local, gols_local, gols_visitant, equip_visitant) values (?, ?, ?, ?);";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, p.getEquipLocal().getId());
            insertStatement.setInt(2, p.getPuntLocal());
            insertStatement.setInt(3, p.getPuntVisitant());
            insertStatement.setInt(4, p.getEquipVisitant().getId());
            insertStatement.execute();
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public List<Partit> list(){
        List<Partit> list = new ArrayList<>();
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "select p.id as id, l.nom as name_local, l.abbr as abbr_local, l.id as id_local, p.gols_local, p.gols_visitant, v.nom as nom_visitant, v.abbr as abbr_visitant, v.id as id_visitant from partit p, equip l, equip v where p.equip_local = l.id and p.equip_visitant = v.id";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int idPartit = resultat.getInt("id");
                int idLocal = resultat.getInt("id_local");
                String nomLocal = resultat.getString("name_local");
                String abbrLocal = resultat.getString("abbr_local");
                Equip local = new Equip(idLocal, nomLocal, abbrLocal);

                int idVisitant = resultat.getInt("id_visitant");
                String nomVisitant = resultat.getString("nom_visitant");
                String abbrVisitant = resultat.getString("abbr_visitant");
                Equip visitant = new Equip(idVisitant, nomVisitant, abbrVisitant);

                int puntsLocal = resultat.getInt("gols_local");
                int puntsVisitant = resultat.getInt("gols_visitant");
                list.add(new Partit(idPartit, local, visitant, puntsLocal, puntsVisitant));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    public boolean delete(int id){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "DELETE FROM partit WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean update(Partit p){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "UPDATE partit SET(equip_local, gols_local, gols_visitant, equip_visitant) = (?, ?, ?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, p.getEquipLocal().getId());
            insertStatement.setInt(2, p.getPuntLocal());
            insertStatement.setInt(3, p.getPuntVisitant());
            insertStatement.setInt(4, p.getEquipVisitant().getId());
            insertStatement.setInt(5, p.getId());
            insertStatement.execute();
            return true;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return false;
    }

    public List<Integer> puntsTotal() {
        List<Integer> punts = new ArrayList<>();
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "select SUM(p.gols_local) as gols_local, sum(p.gols_visitant) as gols_visitant from partit p, equip e where (e.id=p.equip_local or e.id=p.equip_visitant)";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int puntsLocal = resultat.getInt("gols_local");
                int puntsVisitant = resultat.getInt("gols_visitant");
                punts.add(puntsLocal);
                punts.add(puntsVisitant);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return punts;
    }

    public double countPartit() {
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "select count(*) as num_partit from partit p, equip l, equip v where l.id = p.equip_local and v.id = p.equip_visitant;";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            double count = 0;

            while (resultat.next()){
                count = resultat.getInt("num_partit");
            }
            return count;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int maxPartit() {
        int puntsMax = 0;
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "select sum(coalesce(gols_local,0) + coalesce(gols_visitant,0)) as punts_total, id from partit group by id order by punts_total desc limit 1;";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);


            while (resultat.next()){
                puntsMax = resultat.getInt("punts_total");
            }

        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return puntsMax;
    }

    public List<Integer> equipPunts(String nomEquip){
        List<Integer> punts = new ArrayList<>();
        punts.add(0);
        punts.add(0);
        List<Equip> equips = equipDAO.list();
        List<Partit> partits = list();
        for (Equip e: equips) {
            if (e.getNom().equals(nomEquip)){
                for (Partit p: partits) {
                    if (p.getEquipLocal().getNom().equals(nomEquip)){
                        int puntsNous = punts.get(0) + p.getPuntLocal();
                        punts.add(0, puntsNous);
                    }
                    else if(p.getEquipVisitant().getNom().equals(nomEquip)){
                        int puntsNous = punts.get(1) + p.getPuntVisitant();
                        punts.add(1,  puntsNous);
                    }
                }
            }
        }
        return punts;
    }

    public double partitGuanyat(String nom){
        List<Equip> equips = equipDAO.list();
        List<Partit> partits = list();
        int pJugat = 0;
        int pGuanyat = 0;
        for (Equip e: equips) {
            if (e.getNom().equals(nom)) {
                for (Partit p : partits) {
                    if (p.getEquipLocal().getNom().equals(nom)) {
                        pJugat++;
                        if (p.getPuntLocal() > p.getPuntVisitant()) {
                            pGuanyat++;
                        }
                    } else if (p.getEquipVisitant().getNom().equals(nom)) {
                        pJugat++;
                        if (p.getPuntVisitant() > p.getPuntLocal()) {
                            pGuanyat++;
                        }
                    }
                }
            }
        }
        return ((double) pGuanyat) / pJugat;
    }

}
