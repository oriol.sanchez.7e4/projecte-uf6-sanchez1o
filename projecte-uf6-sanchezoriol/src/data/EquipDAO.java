package data;

import models.Equip;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EquipDAO {
    public void insert(Equip e){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "INSERT INTO equip(nom, abbr) VALUES(?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, e.getNom());
            insertStatement.setString(2, e.getAbreviacio());
            insertStatement.execute();
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    public List<Equip> list(){
        List<Equip> list = new ArrayList<>();
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "SELECT * FROM equip";
            Statement listStatement = connection.createStatement();
            ResultSet resultat = listStatement.executeQuery(query);

            while (resultat.next()){
                int id = resultat.getInt("id");
                String nom = resultat.getString("nom");
                String abbr = resultat.getString("abbr");
                list.add(new Equip(id, nom, abbr));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    public boolean delete(int id){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "DELETE FROM equip WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public boolean update(Equip e){
        try{
            Connection connection = LligaDatabase.getInstance().getConnection();
            String query = "UPDATE equip SET(nom, abbr) = (?, ?) WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, e.getNom());
            insertStatement.setString(2, e.getAbreviacio());
            insertStatement.setInt(3, e.getId());
            insertStatement.execute();
            return true;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return false;
    }
}
