CREATE TABLE partit(
    id serial,
    equip_local integer,
    gols_local numeric,
    gols_visitant numeric,
    equip_visitant integer,
    PRIMARY KEY (id),
    FOREIGN KEY (equip_local) REFERENCES equip(id),
    FOREIGN KEY (equip_visitant) REFERENCES equip(id)
);

CREATE TABLE equip(
    nom varchar(64),
    abbr varchar(6),
    id serial,
    PRIMARY KEY (id)
);

select * from partit;
select * from equip;


insert into equip (nom, abbr) values ('Warriors','GSW');
insert into equip (nom, abbr) values ('Lakers','LA');
insert into equip (nom, abbr) values ('Timberwolves', 'MT');
insert into equip (nom, abbr) values ('Rockets','HR');

insert into partit (equip_local, gols_local, gols_visitant, equip_visitant) values (3, 115, 123, 4);
insert into partit (equip_local, gols_local, gols_visitant, equip_visitant) values (4, 214, 166, 5);
insert into partit (equip_local, gols_local, gols_visitant, equip_visitant) values (5, 147, 159, 6);
insert into partit (equip_local, gols_local, gols_visitant, equip_visitant) values (6, 123, 165, 3);



select p.id, l.nom, l.abbr, l.id, p.gols_local, p.gols_visitant, v.nom, v.abbr, v.id
from partit p, equip l, equip v
where p.equip_local = l.id and p.equip_visitant = v.id;
