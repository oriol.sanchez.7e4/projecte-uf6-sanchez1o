import data.LligaDatabase;
import java.util.Scanner;
import ui.MenuPrincipal;
import ui.MyScanner;

public class Main {
    public static void main(String[] args) throws Exception {
        LligaDatabase.getInstance().connect();

        MyScanner scanner = new MyScanner();
        MenuPrincipal menuPrincipal = new MenuPrincipal(scanner);
        menuPrincipal.showMenu();

        LligaDatabase.getInstance().close();
    }
}