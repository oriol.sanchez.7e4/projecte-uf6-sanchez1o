package ui;

import data.EquipDAO;
import data.PartitDAO;
import models.Equip;
import models.Partit;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class MenuPrincipal extends Menu {
    private EquipDAO equipDAO;
    private PartitDAO partitDAO;

    public MenuPrincipal(MyScanner scanner) {
        super(scanner);
        equipDAO = new EquipDAO();
        partitDAO = new PartitDAO();
        addOption("Llista equips.");
        addOption("Insertar equip.");
        addOption("Editar equip.");
        addOption("Eliminar equip.");
        addOption("Llista partits.");
        addOption("Insertar partit.");
        addOption("Editar partit.");
        addOption("Eliminar partit.");
        addOption("Menu Estadistiques.");
    }

    @Override
    public void action(int selected) throws Exception{
        switch (selected){
            case 1:
                listEquip();
                break;
            case 2:
                insertEquip();
                break;
            case 3:
                updateEquip();
                break;
            case 4:
                deleteEquip();
                break;
            case 5:
                listPartit();
                break;
            case 6:
                insertPartit();
                break;
            case 7:
                updatePartit();
                break;
            case 8:
                deletePartit();
                break;
            case 9:
                MenuEstadistica menuEstadistica = new MenuEstadistica(scanner);
                menuEstadistica.showMenu();
                break;
        }
        System.out.println();
    }

    public void listEquip() throws SQLException, InterruptedException {
        List<Equip> equips = equipDAO.list();
        equips.forEach(equip -> System.out.printf(" %s\n", equip));
    }
    public void insertEquip(){
        String blanc = scanner.nextLine();
        System.out.print("Introdueix nom del equip: ");
        String nom = scanner.nextLine();
        System.out.print("Introdueix abreviació del equip: ");
        String abbr = scanner.nextLine();
        equipDAO.insert(new Equip(nom, abbr));
        System.out.println();
    }
    public void updateEquip() throws SQLException, InterruptedException {
        listEquip();
        System.out.print("Introdueix la id del equip: ");
        int id = scanner.nextInteger();
        System.out.print("Introdueix el nom del equip: ");
        String blanc = scanner.nextLine();
        String nom = scanner.nextLine();
        System.out.print("Introdueix l'abreviació del equip: ");
        String abbr = scanner.nextLine();
        equipDAO.update(new Equip(id, nom, abbr));
        System.out.println();
    }
    public void deleteEquip() throws SQLException, InterruptedException {
        listEquip();
        System.out.print("Introdueix id de l'equip: ");
        int id = scanner.nextInteger();
        equipDAO.delete(id);
        System.out.println();
    }

    private void listPartit() throws SQLException {
        List<Partit> partits = partitDAO.list();
        partits.forEach(partit -> System.out.printf(" %s\n", partit));
        System.out.println();
    }

    public Equip getEquipByNom(String nom){
        List<Equip> equips = equipDAO.list();
        for (Equip e: equips) {
            if (e.getNom().equals(nom)){
                return e;
            }
        }
        return null;
    }

    public void insertPartit() throws SQLException, InterruptedException {
        listEquip();
        String blanc = scanner.nextLine();
        System.out.print("Introdueix nom del equip Local: ");
        String equipLocal = scanner.nextLine();
        System.out.print("Introdueix nom del equip Visitant: ");
        String equipVisitant = scanner.nextLine();
        System.out.print("Introdueix els punts del equip Local: ");
        int puntsLocal = scanner.nextInteger();
        System.out.print("Introdueix els punts del equip Visitant: ");
        int puntsVisitant = scanner.nextInteger();

        Equip local = getEquipByNom(equipLocal);
        Equip visitant = getEquipByNom(equipVisitant);

        partitDAO.insert(new Partit(local, visitant, puntsLocal, puntsVisitant));
        System.out.println();
    }
    public void updatePartit() throws SQLException {
        listPartit();
        System.out.print("Introdueix la id del partit: ");
        int id = scanner.nextInteger();
        String blanc = scanner.nextLine();
        System.out.print("Introdueix el nom del equip Local: ");
        String nomLocal = scanner.nextLine();
        System.out.print("Introdueix el nom del equip Visitant: ");
        String nomVisitant = scanner.nextLine();
        System.out.print("Introdueix els punts Local: ");
        int puntsLocal = scanner.nextInteger();
        System.out.print("Introdueix els punts Visitant: ");
        int puntsVisitant = scanner.nextInteger();

        partitDAO.update(new Partit(id, getEquipByNom(nomLocal), getEquipByNom(nomVisitant), puntsLocal, puntsVisitant));
        System.out.println();
    }
    public void deletePartit() throws SQLException {
        listPartit();
        System.out.print("Introdueix id de l'equip: ");
        int id = scanner.nextInteger();
        partitDAO.delete(id);
        System.out.println();
    }
}
