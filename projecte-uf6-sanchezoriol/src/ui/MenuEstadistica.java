package ui;

import data.EquipDAO;
import data.PartitDAO;
import models.Equip;
import models.Partit;

import java.sql.SQLException;
import java.util.List;

public class MenuEstadistica {
    private EquipDAO equipDAO;
    private PartitDAO partitDAO;
    private MyScanner scanner;
    private MenuPrincipal principal;

    public MenuEstadistica(MyScanner scanner){
        this.scanner = scanner;
        equipDAO = new EquipDAO();
        partitDAO = new PartitDAO();
        principal = new MenuPrincipal(scanner);
    }

    public void showMenu() throws SQLException, InterruptedException {
        while(true) {
            System.out.println("Menu ESTADISTIQUES");
            System.out.println("Quina operació vols realitzar?");
            System.out.println("1) Punts totals lliga");
            System.out.println("2) Mitja punts per partit a la lliga");
            System.out.println("3) Punts màxims lliga");
            System.out.println("4) Punts totals d'un Equip");
            System.out.println("5) Mitja punts d'un Equip");
            System.out.println("6) Ratio de victoria d'un Equip");
            System.out.println("0) Sortir");
            System.out.print("Introdueix la opció: ");
            int accio = scanner.nextIntMax(6);


            switch (accio) {
                case 1:
                    puntsTotalsLliga();
                    break;
                case 2:
                    mitjanaLliga();
                    break;
                case 3:
                    puntsMaxLliga();
                    break;
                case 4:
                    getPuntsTotalEquip();
                    break;
                case 5:
                    puntsMitjanaEquip();
                    break;
                case 6:
                    ratioVictoria();
                    break;
                case 0:
                    return;
            }
        }
    }

    private void puntsTotalsLliga(){
        List<Integer> punts = partitDAO.puntsTotal();
        System.out.printf("Punts totals a la lliga: %d punts\n", punts.get(0) + punts.get(1));
        System.out.println();
    }

    private void mitjanaLliga(){
        List<Integer> punts = partitDAO.puntsTotal();
        System.out.println((punts.get(0) + punts.get(1))/partitDAO.countPartit());

        System.out.printf("Mitjana de punts per partit: %.2f punts\n", (punts.get(0) + punts.get(1))/partitDAO.countPartit());
        System.out.println();
    }

    private void puntsMaxLliga(){
        System.out.printf("Els punts totals màxims d'un partit és: %d\n", partitDAO.maxPartit());
        System.out.println();
    }

    private void getPuntsTotalEquip() throws SQLException, InterruptedException {
        principal.listEquip();
        System.out.println("Introdueix el nom del equip: ");
        String nomEquip = scanner.next();
        List<Integer> punts = partitDAO.equipPunts(nomEquip);
        System.out.printf("L'equip té %d punts\n", punts.get(0) + punts.get(1));
    }

    private void puntsMitjanaEquip() throws SQLException, InterruptedException {
        principal.listEquip();
        System.out.println("Introdueix equip: ");
        String nomEquip = scanner.next();
        List<Integer> punts = partitDAO.equipPunts(nomEquip);
        double mitjana = (punts.get(0)+punts.get(1))/partitDAO.countPartit();
        System.out.printf("L'equip té una mitjana de %.2f punts per partit\n", mitjana);
        System.out.println();
    }

    private void ratioVictoria() throws SQLException, InterruptedException {
        principal.listEquip();
        System.out.println("Introdueix equip: ");
        String equipNou = scanner.next();
        System.out.printf("El ratio de Victories de l'equip és: %.2f%%\n", partitDAO.partitGuanyat(equipNou));
        System.out.println();
    }
}
