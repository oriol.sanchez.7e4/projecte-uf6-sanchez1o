package ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Menu {
    List<String> options;
    MyScanner scanner;

    public Menu(MyScanner scanner){
        this.scanner = scanner;
        this.options = new ArrayList<>();
    }

    protected void addOption(String option){
        options.add(option);
    }


    int selectOption(){
        System.out.print("Introdueix l'opció: ");
        int n = scanner.nextInteger();
        while (n < 0 && n > options.size()){
            System.err.println("Opció no vàlida.");
            System.out.print("Introdueix l'opció: ");
            n = scanner.nextInteger();
        }
        return n;
    }

    public void showMenu() throws Exception {
        while(true) {
            for (int i = 0; i < options.size(); i++)
                System.out.printf("%d) %s\n", i + 1, options.get(i));
            System.out.println("0) Sortir.");
            int selected = selectOption();
            if(selected == 0)
                break;
            else
                action(selected);
        }
    }

    public abstract void action(int selected) throws Exception;
}
