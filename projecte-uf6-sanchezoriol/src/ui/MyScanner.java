package ui;

import java.util.Locale;
import java.util.Scanner;

public class MyScanner {
    Scanner scanner;

    public MyScanner(){
        this.scanner = new Scanner(System.in).useLocale(Locale.US);
    }

    /**
     * Mètode que demana un nombre a l'usuari, en el rang 0 a nombreMaxim
     * @param nombreMaxim Nombre màxim de opcions en el Menú
     * @return
     */
    public int nextIntMax(int nombreMaxim){
        int accio = scanner.nextInt();
        while (accio < 0 || accio > nombreMaxim) {
            System.out.println("ERROR! L'opció introuida està fora del rang.");
            accio = scanner.nextInt();
        }
        return accio;
    }

    public String nextLine(){
        return scanner.nextLine();
    }
    public int nextInteger(){
        return scanner.nextInt();
    }

    public Scanner getDefaultScanner(){
        return scanner;
    }

    public String next() {
        return scanner.next();
    }
}
