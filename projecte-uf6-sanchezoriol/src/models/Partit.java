package models;

public class Partit {
    private int id;
    private Equip equipLocal;
    private int puntLocal;
    private int puntVisitant;
    private Equip equipVisitant;

    public Partit(Equip equipLocal,  Equip equipVisitant, int puntLocal, int puntVisitant) {
        this.equipLocal = equipLocal;
        this.puntLocal = puntLocal;
        this.puntVisitant = puntVisitant;
        this.equipVisitant = equipVisitant;
    }
    public Partit(int id, Equip equipLocal,  Equip equipVisitant, int puntLocal, int puntVisitant) {
        this.id = id;
        this.equipLocal = equipLocal;
        this.puntLocal = puntLocal;
        this.puntVisitant = puntVisitant;
        this.equipVisitant = equipVisitant;
    }

    public int getId() {
        return id;
    }
    public Equip getEquipLocal() {
        return equipLocal;
    }
    public int getPuntLocal() {
        return puntLocal;
    }
    public int getPuntVisitant() {
        return puntVisitant;
    }
    public Equip getEquipVisitant() {
        return equipVisitant;
    }

    public void setEquipLocal(Equip equipLocal) {
        this.equipLocal = equipLocal;
    }
    public void setPuntLocal(int puntLocal) {
        this.puntLocal = puntLocal;
    }
    public void setPuntVisitant(int puntVisitant) {
        this.puntVisitant = puntVisitant;
    }
    public void setEquipVisitant(Equip equipVisitant) {
        this.equipVisitant = equipVisitant;
    }

    @Override
    public String toString() {
        return String.format("%d) %s %d - %d %s", getId(), getEquipLocal().getNom(), getPuntLocal(), getPuntVisitant(), getEquipVisitant().getNom());
    }
}
